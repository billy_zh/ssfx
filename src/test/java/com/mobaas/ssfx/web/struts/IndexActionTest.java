package com.mobaas.ssfx.web.struts;

import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;

import org.apache.struts2.StrutsSpringJUnit4TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.mobaas.ssfx.config.AppConfig;
import com.opensymphony.xwork2.Action;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class)
@WebAppConfiguration
public class IndexActionTest extends StrutsSpringJUnit4TestCase<IndexAction> {

	private IndexAction getAction(String url) {
		return (IndexAction)getActionProxy(url).getAction();
	}
	
	@Test
	public void testIndex() throws Exception {
		
		String result = getAction("/index.do").index();
		
		Assert.assertEquals(Action.SUCCESS, result);
	}
	
	@Test
	public void testLogin() throws Exception {
		
		request.setAttribute(CsrfToken.class.getName(), new DefaultCsrfToken("_csrf", "_csrf", UUID.randomUUID().toString()));
		
		String result = getAction("/login.do").login();
		
		Assert.assertEquals(Action.SUCCESS, result);
	}

	
	
}
