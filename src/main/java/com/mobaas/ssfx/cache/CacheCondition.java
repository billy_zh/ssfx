/*
 * Copyright 2009-2016 Billy Zhang
 */
package com.mobaas.ssfx.cache;

import java.util.Properties;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.mobaas.ssfx.util.PropertiesUtil;

/**
 * Cache 条件注解
 * @author billy (billy_zh@126.com)
 *
 */
@Configuration
public abstract class CacheCondition implements Condition {

	private static final String EHCACHE = "ehcache";
	private static final String REDIS = "redis";
	private static final String CACHE_PROVIDER = "cache.provider";
	
	protected abstract String getName();
	
	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
	
		Properties props = PropertiesUtil.load(context.getEnvironment());
		if ( props.containsKey(CACHE_PROVIDER) ) {
			return getName().equals( props.getProperty(CACHE_PROVIDER) );
		}
		
		return EHCACHE.equals(getName());  // default
	}
	
	public final static class Redis extends CacheCondition {
		
		@Override
		public String getName() {
			return REDIS;
		}
	}

	public final static class EhCache extends CacheCondition {
		
		@Override
		public String getName() {
			return EHCACHE;
		}
	};
	
	
}
