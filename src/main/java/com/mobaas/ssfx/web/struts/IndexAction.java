/*
 * Copyright 2009-2016 Billy Zhang
 */
package com.mobaas.ssfx.web.struts;

import org.springframework.context.annotation.Scope;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * Index 动作类
 * @author billy (billy_zh@126.com)
 *
 */
@Component
@Scope("prototype")
public class IndexAction extends BaseAction {

	public String index() throws Exception {
		
		return SUCCESS;
	}
	
	public String login() throws Exception {
		
		CsrfToken csrfToken = (CsrfToken) getAttribute(CsrfToken.class.getName());
	    putValue("token", csrfToken.getToken());
	    
		return SUCCESS;
	}
}
