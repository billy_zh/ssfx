/*
 * Copyright 2009-2016 Billy Zhang
 */
package com.mobaas.ssfx.web.struts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.mobaas.ssfx.PageList;
import com.mobaas.ssfx.Pager;
import com.mobaas.ssfx.domain.AdminLog;
import com.mobaas.ssfx.domain.Administrator;
import com.mobaas.ssfx.service.AdminService;
import com.mobaas.ssfx.util.CryptoUtil;
import com.mobaas.ssfx.web.JsonResult;
import com.mobaas.ssfx.web.aop.OpCategory;
import com.mobaas.ssfx.web.aop.OperateLog;


/**
 * 管理员动作类
 * @author billy (billy_zh@126.com)
 *
 */
@Component
@Scope("prototype")
public class AdminAction extends BaseAction {

	@Autowired
	private AdminService adminService;
	
	private int id;
	public void setId(int id) { this.id = id; }
	
	public String list() throws Exception {
		
		Pager pager = new Pager(getPageNo());
		
		String keyword = getParameter("keyword", "");
		
		PageList<Administrator> plist = adminService.selectAdminList(keyword, pager);
		pager.setRowCount(plist.getTotal());
		putValue("adminlist", plist.getList());
		putValue("pager", pager);
		
		return SUCCESS;
	}
	
	public String loglist() throws Exception {
		
		Pager pager = new Pager(getPageNo());

		String category = getParameter("category", "");
		
		PageList<AdminLog> plist = adminService.selectAdminLogList(category, pager);
		pager.setRowCount(plist.getTotal());
		putValue("loglist", plist.getList());
		putValue("pager", pager);
		putValue("category", category);
		
		return SUCCESS;
	}

	public String add() throws Exception {
		
		return SUCCESS;
	}

	@OperateLog(Category=OpCategory.SYSTEM, Name="添加管理员")
	public String add_submit() throws Exception {
		
		String loginName = getParameter("loginName");
		String password = getParameter("password");
		String repPasswd = getParameter("repPasswd");
		
		if (StringUtils.isEmpty(loginName)) {
			setJsonResult( JsonResult.error(-1, "用户名不能为空") );
			return INPUT;
		}

		if (StringUtils.isEmpty(password)) {
			setJsonResult( JsonResult.error(-1, "密码不能为空") );
			return INPUT;
		}
		if (StringUtils.isEmpty(repPasswd)) {
			setJsonResult( JsonResult.error(-1, "重复密码不能为空") );
			return INPUT;
		}
		
		if (!password.equals(repPasswd)) {
			setJsonResult( JsonResult.error(-1, "密码不一致") );
			return INPUT;
		}
		
		Administrator admin = new Administrator();
		admin.setLoginName(loginName);
		admin.setPassword(CryptoUtil.MD5(password));
		adminService.insertAdmin(admin);
		
		setJsonResult( JsonResult.ok() );
		return SUCCESS;
	}
	
	public String edit() throws Exception {

		Administrator admin = adminService.selectAdminById(id);
		if (admin != null) {
			
			putValue("admin", admin);
			
			return SUCCESS;
		}

		return ERROR;
	}

	@OperateLog(Category=OpCategory.SYSTEM, Name="编辑管理员")
	public String edit_submit() throws Exception {
		
		Administrator originAdmin = adminService.selectAdminById(id);
		if (originAdmin != null) {
			
			String newPasswd = getParameter("newPasswd");
			
			if (!(newPasswd == null || newPasswd.equals(""))) {
				originAdmin.setPassword(CryptoUtil.MD5(newPasswd));
			}

			int n = adminService.updateAdmin(originAdmin);
			
			setJsonResult( JsonResult.ok() );
			return SUCCESS;
		}

		return ERROR;
	}

	@OperateLog(Category=OpCategory.SYSTEM, Name="删除管理员")
	public String delete() throws Exception {

		String confirm = getParameter("confirm");
		if (StringUtils.isEmpty(confirm)) {
			putValue("id", id);
			
			return "confirm";
		} else if ("yes".equals(confirm)) {
			
			int n = adminService.deleteAdmin(id);
			
			setJsonResult( JsonResult.ok() );
			return SUCCESS;
		}
		
		return ERROR;
	}
}
