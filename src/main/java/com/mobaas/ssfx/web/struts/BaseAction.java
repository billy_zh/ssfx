/*
 * Copyright 2009-2016 Billy Zhang
 */
package com.mobaas.ssfx.web.struts;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.mobaas.ssfx.web.JsonResult;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 动作基类
 * @author billy (billy_zh@126.com)
 *
 */
public abstract class BaseAction extends ActionSupport {

	private String result;
	private JsonResult jsonResult;
	
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public JsonResult getJsonResult() {
    	return  jsonResult;
    }
    
    public void setJsonResult(JsonResult jsonResult) {
    	this.jsonResult = jsonResult;
    }
    
    protected void putValue(String key, Object value) {
    	ServletActionContext.getContext().put(key, value);;
    }
    
    protected ServletContext getServletContext() {
		return ServletActionContext.getServletContext();
	}
    
    protected HttpSession getSession() {
		return ServletActionContext.getRequest().getSession();
	}
    
    public String getRemoteAddr() {
		return ServletActionContext.getRequest().getRemoteAddr();
	}
	
	public String getParameter(String name) {
		return ServletActionContext.getRequest().getParameter(name);
	}
	
	public String getParameter(String name, String defaultValue) {
		String param = ServletActionContext.getRequest().getParameter(name);
		return param != null ? param : defaultValue;
	}
	
	public Object getAttribute(String name) {
		return ServletActionContext.getRequest().getAttribute(name);
	}
	
	protected int getPageNo() {
		int pageNo = 1;
		
		String s = getParameter("page");
		if (s != null && s.length() > 0) {
			try {
				pageNo = Integer.parseInt(s);
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
			}
		}
		
		return pageNo;
	}
}
