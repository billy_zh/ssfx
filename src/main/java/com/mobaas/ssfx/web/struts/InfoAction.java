/*
 * Copyright 2009-2016 Billy Zhang
 */
package com.mobaas.ssfx.web.struts;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;

import com.mobaas.ssfx.PageList;
import com.mobaas.ssfx.Pager;
import com.mobaas.ssfx.domain.Info;
import com.mobaas.ssfx.service.InfoService;
import com.mobaas.ssfx.web.aop.OpCategory;
import com.mobaas.ssfx.web.aop.OperateLog;

/**
 * 资讯动作类
 * @author billy (billy_zh@126.com)
 *
 */
@Component
@Scope("prototype")
public class InfoAction extends BaseAction {

	@Autowired
	private InfoService infoService;
	
	private int id;
	public void setId(int id) { this.id = id; }

	private String getCsrfToken() {
		CsrfToken csrfToken = (CsrfToken) getAttribute(CsrfToken.class.getName());
	    return csrfToken.getToken();
	}
	
	public String list() throws Exception {
		
		Pager pager = new Pager(getPageNo());
		
		String keyword = getParameter("keyword", "");
		
		PageList<Info> plist = infoService.selectInfoList(keyword, pager);
		pager.setRowCount(plist.getTotal());
		putValue("infolist", plist.getList());
		putValue("keyword", keyword);
		putValue("pager", pager);
		
		return SUCCESS;
	}
	
	public String add() throws Exception {

		putValue("token", getCsrfToken());
		return SUCCESS;
	}

	@OperateLog(Category=OpCategory.INFO, Name="添加资讯")
	public String add_submit() throws Exception {
		
		Info info = new Info();
		info.setTitle( getParameter("title") );
		info.setContent( getParameter("content") );
		info.setPubTime(new Date());

		infoService.insertInfo(info);
		
		return SUCCESS;
	}
	
	public String edit() throws Exception {
		
		Info info = infoService.selectInfoById(id);
		if (info != null) {
			putValue("info", info);
		}

		putValue("token", getCsrfToken());
		return SUCCESS;
	}

	@OperateLog(Category=OpCategory.INFO, Name="编辑资讯")
	public String edit_submit() throws Exception {

		Info info = infoService.selectInfoById(id);
		if (info != null) {

			info.setTitle( getParameter("title") );
			info.setContent( getParameter("content") );
			
			infoService.updateInfo(info);
		}
		
		return SUCCESS;
	}
}
