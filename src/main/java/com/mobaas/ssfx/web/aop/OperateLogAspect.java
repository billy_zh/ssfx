package com.mobaas.ssfx.web.aop;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.mobaas.ssfx.Constants;
import com.mobaas.ssfx.domain.AdminLog;
import com.mobaas.ssfx.service.AdminService;
import com.mobaas.ssfx.web.struts.BaseAction;

/**
 * 操作日志AOP切面类
 * @author billy (billy_zh@126.com)
 *
 */
@Aspect
@Component
public class OperateLogAspect {

	/**
	 * 日志切入点
	 */
	@Pointcut(Constants.LOG_POINTCUT)
	private void log(){} //声明一个切入点,切入点的名称其实是一个方法
	
    private long startTimeMillis = 0; // 开始时间  
    private int duration = 0; // 

    @Autowired
    private AdminService adminService;
    
    /** 
     * 方法调用前触发
     * @param joinPoint 
     */ 
    @Before("log()")
    public void before(JoinPoint joinPoint){  
        startTimeMillis = System.currentTimeMillis(); //记录方法开始执行的时间  
    }  
    
    /**
     * 方法调用后触发
     * @param joinPoint
     */
    @After("log()")
    public void after(JoinPoint joinPoint){  
        duration = (int)(System.currentTimeMillis() - startTimeMillis);  
    }
    
    /** 
     * 方法调用后触发
     * @param joinPoint 
     * @param result 方法返回值。
     */
    @AfterReturning(pointcut="log()", returning="result")
    public void afterReturning(JoinPoint joinPoint, String result) { 

        Class<?> targetClass = joinPoint.getTarget().getClass();  
        String methodName = joinPoint.getSignature().getName();  
        Object[] arguments = joinPoint.getArgs(); 

        Class<?>[] argClass = new Class<?>[arguments.length];
        for (int i=0; i<arguments.length; i++) {
        	argClass[i] = arguments[i].getClass();
        }

        Method targetMethod = null;
		try {
			targetMethod = targetClass.getMethod(methodName, argClass);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
        if (targetMethod != null) {
        	OperateLog opLog = targetMethod.getAnnotation(OperateLog.class);
        	if (opLog != null) {
        		AdminLog log = new AdminLog();
        		log.setCategory( opLog.Category().toString() );
        		log.setOpName( opLog.Name() );
        		log.setDuration(duration);
        		log.setResult(result);
        		log.setLogTime(new Date());
        		
        		log.setLoginName(SecurityContextHolder.getContext().getAuthentication().getName());
        		
        		if (joinPoint.getTarget() instanceof BaseAction) {
        			BaseAction ba = (BaseAction)joinPoint.getTarget();
        			log.setLogIp(ba.getRemoteAddr());
        		}
        		
        		adminService.insertAdminLog(log);
        	}
        }

    }
  
    /**
     * 异常触发
     * @param joinPoint
     * @param ex
     */
    @AfterThrowing(pointcut="log()", throwing="ex")
    public void afterThrowing(JoinPoint joinPoint, Exception ex) {
    	
    }
    
    /** 
     * 环绕触发
     * @param joinPoint 
     * @return Object
     * @throws Throwable 
     */  
    //@Around("log()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {  

        return null;  
    }  

}
