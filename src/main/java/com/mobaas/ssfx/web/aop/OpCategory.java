package com.mobaas.ssfx.web.aop;

/**
 * 操作日志类别
 * @author billy (billy_zh@126.com)
 *
 */
public enum OpCategory {

	NONE,

	INFO,

	SYSTEM
}
