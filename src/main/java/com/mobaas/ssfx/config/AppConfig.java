/*
 * Copyright 2009-2016 Billy Zhang
 */
package com.mobaas.ssfx.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.cache.annotation.EnableCaching;

import com.mobaas.ssfx.Constants;

/**
 * 应用配置类
 * @author billy (billy_zh@126.com)
 *
 */
@Configuration
@EnableCaching
@EnableAspectJAutoProxy(proxyTargetClass=true)
@ComponentScan(Constants.COMPONENT_PACKAGE)
public class AppConfig {
	
	@Bean
	public DataSource dataSource(AppProperties appProperties) {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName( appProperties.getString("jdbc.driverClassName") );
		dataSource.setUrl( appProperties.getString("jdbc.url") );
		dataSource.setUsername( appProperties.getString("jdbc.username") );
		dataSource.setPassword( appProperties.getString("jdbc.password") );
		dataSource.setMaxActive( appProperties.getInteger("jdbc.maxActive") );
		dataSource.setMaxWait( appProperties.getInteger("jdbc.maxWait") );
		dataSource.setMaxIdle( appProperties.getInteger("jdbc.maxIdle") );
		dataSource.setMinIdle( appProperties.getInteger("jdbc.minIdle") );
		
		return dataSource;
	}
}
