# ssfx

#### 项目介绍
Spring Struts 开发框架

SpringFramework4 JavaConfig配置方式

WEB采用Struts2 由Spring创建Action Bean.

安全采用Spring Security

数据访问支持MyBatis，Hibernate，可通过配置项切换

数据缓存支持EhCache，Redis，可通过配置项切换

#### 软件架构
MVC三层架构
